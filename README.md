# Python en Docker


#### Construir la imagen:

    docker build -t [name] .

*-t [name] = tag (poner una etiqueta para identificar la imagen)*
*. = le decimos a docker donde esta el dockerfile*

-----


### Correr la imagen:

    docker run [name]
    
*[name] = el -t [name] con el que nombramos la imagen*

-----


### Correr la imagen de nuevo:

*Ver los contenedores corriendo:*

    docker ps

*Ver los contenedores que no estan corriendo:*

    docker ps -a (ver todos los contenedores que tenemos)

*Correr el contenedor:*

    docker start -ia [hash o name]

*-ia inateractive attach = STD input, output y error para ver el resultado*

-----


### Volumen:

    docker run -v /local/path:/container/path

*Agregar el volume al dockerfile:*

    VOLUME /directorio_compartido

-----


### Puertos:

    docker run -p 8888:8080 (local:container)

*agregar los puertos al dockerfile:*

    EXPOSE 8080

-----


### Ciclo de vida de un contenedor:

* Concepcion: build

* nacimiento: run

* reproduccion: commit

* sueño: kill

* despertar: wake

* death: rm (remover container)

* extincion: rmi (remover imagen)

-----


### Example:


    # Use image
    FROM python
    # Meta data
    LABEL maintainer="setcain <setcain@mail.com>" \
          description="a little description of my container"
    # set working dir in container
    WORKDIR /app
    # install all you need
    RUN pip install something \
        pip install something else
    # make a port of container available from outside
    EXPOSE 8080
    # save info
    VOLUME /app
    # run commands
    ENTRYPOINT ["python"]
    CMD ["myapp.py"]

*docker build -t myapp .*
*docker run -p 8888:8080 -v /Users/setcain/info:/app myapp*
