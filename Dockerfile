# image
FROM python:3.6.9-slim-buster

# set working directory in local
WORKDIR /app

# copy all content from local to container
COPY . /app

# run command for start the application
CMD ["python", "hello_world.py"]

